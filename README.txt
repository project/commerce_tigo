This project creates a Tigo Money payment gateway for the Drupal Commerce
payment and checkout systems.

Tigo Money is a payment gateway that uses only your cellphone number to pay.

It currently supports Tigo Money in Bolivia (http://www.tigo.com.bo/tigo-money).
Future work can be done to implement Tigo Money in Guatemala
(http://www.tigo.com.gt/tigo-money) or other countries.
